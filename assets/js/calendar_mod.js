
function init() {
    /*どちらも、ドラッグ可能＆挿入可能だが、*/
     // 移動適用
    $(".targetDiv").draggable({
        containment: "#container",
        scorp : "hoge"
    });
    // リサイズ適用
    $(".targetDiv").resizable({
        containment: "#container"
    });

    var hoge = document.getElementsByClassName("targetDiv");
    /*一つ一つ、要素に対しabsolute化する（*/
    for(var i = 0 ; i < hoge.length ; i++ ){
        hoge[i].style.position = "absolute";
    }

}



$(document).ready(function(){
    init();//初期化
    var sel = new selectedTeachersList();//crud処理のオブジェクトを参照
    //sel.resetPost();
    /*追加処理。*/
    $("#add_obj").click(function(){
        var comment = document.getElementById("desc").value;
        var img = sel.post.stamp_path;//画像のパスを取得する
        /*投稿に対する処理　コメントか画像があれば、投稿できる*/
        if ( comment != "" || img != "" ){
            /*値の挿入*/
             sel.addDom(comment,img);
             sel.addDomList(comment,img);
             init();//初期化
             sel.resetForm();
        }else{
            alert("empty!");//からの時の対応
        }

    });

    /*
        スタンプが選択された時の処理
    */
    $(".stamp_list").click(function(){
        sel.imgSelect($(this).attr("id"));
    });


    /*要素クリック*/
    $(".targetDiv").click(function(){
       console.log("editするとか？");
    });

});

/*
    選択されている講師一覧リストをCRUDする処理を書いておく
    これ、長くしすぎるのも問題か。
*/
var selectedTeachersList = function(){

  this.conf = {};
  this.conf.parent_id = "events";//追加する際のターゲットを指定する
  this.conf.parent_list_id = "calendar_list";//追加する際のターゲットを指定する
  this.conf.w = "100px";
  this.conf.h = "100px";

  this.resetPost();
}

/*イベントの編集に使うパラメータ*/
selectedTeachersList.prototype.resetPost = function(){

    this.post = {};
    this.post.add_allow = false;//新規投稿できるかどうか
    this.post.edit_allow = false;//新規投稿できるかどうか
    this.post.del_allow = false;//編集できるかの問題
    this.post.string = "";//文字部分（文字か画像かのどれかは入ってないとダメ）
    this.post.stamp_path = "";//選択されているスタンプのパス
    this.post.stamp_dom_id = "";//選択されているスタンプのドム
}

/*どんなオブジェクトが入っているか？を保存する*/
selectedTeachersList.prototype.resetObjList = function(){
    /*オブジェクトリストを、空の連想配列として用意する*/
    this.obj_list = {};
}

/*どんなオブジェクトが入っているか？を保存する*/
selectedTeachersList.prototype.addObj = function(tmp_obj){
    /*オブジェクトリストを*/
    this.obj_list[tmp_obj.id];
}

/*フォーム部分の初期化処理*/
selectedTeachersList.prototype.resetForm = function(){
    //console.log("resetform");
    document.getElementById("desc").value = "";
    /*スタンプ設定があれば、消去する*/
    if ( this.post.stamp_dom_id != "" ) document.getElementById(this.post.stamp_dom_id).classList.remove("selected_stamp");
    this.resetPost();

}



/*
    スタンプ選択処理
*/
selectedTeachersList.prototype.imgSelect = function(id){

    /*もし、クラスが設定されていれば対象をトグって削除する*/
    if ( this.post.stamp_dom_id != $("#"+id).attr("id") ){

        if ( this.post.stamp_dom_id != "" ){
            $("#"+this.post.stamp_dom_id).toggleClass("selected_stamp");
        }

        this.post.stamp_dom_id = id;
        this.post.stamp_path = $("#"+id).attr("src");
        $("#"+id).addClass("selected_stamp");
    }
}

/*画像部分のドムを生成する
  引数　指定したパス　戻り値　画像ＵＲＬなどを入れたドム
*/
selectedTeachersList.prototype.docDom = function(id,comment){
  var div_obj = document.createElement("div");
  var a_obj = document.createElement("a");
  a_obj.setAttribute("href","#");

  a_obj.appendChild(document.createTextNode(comment));
  div_obj.appendChild(a_obj);
  return div_obj;
}

/*
    追加処理を書いている
*/
selectedTeachersList.prototype.addDom = function(desc,img){


  /*対象の下にタグを生成していく。*/
  var target = document.getElementById(this.conf.parent_id);

    /*DOMを作っていく*/
    var div_obj = document.createElement("div");
    div_obj.classList.add("targetDiv");
    /*もし、画像があれば*/
    if ( img != "" ) {
        div_obj.style.backgroundImage = "url("+img+")";
        div_obj.style.backgroundSize = "cover";
    }

    div_obj.appendChild(document.createTextNode(desc));
    target.appendChild(div_obj);

}

/*リストに入れる*/
selectedTeachersList.prototype.addDomList = function(desc,img){

    /*仮のオブジェクトを用意する*/
    tmp_obj = {};
    tmp_obj.order = this.teache;

    /*リストの下に発生する要素*/
    var target = document.getElementById(this.conf.parent_list_id);

    /*DOMを作っていく*/
    var tr_obj = document.createElement("tr");
    /*ID要素*/
    var td_obj = document.createElement("td");
    td_obj.appendChild(document.createTextNode("-"));
    tr_obj.appendChild(td_obj);

    /*文書の要素*/
    var td_obj = document.createElement("td");
    td_obj.appendChild(document.createTextNode(desc));
    tr_obj.appendChild(td_obj);

    /*画像の要素*/
    var td_obj = document.createElement("td");


    /*もし、画像があれば*/
    if ( img != "" ) {
        var img_obj = document.createElement("img");
        img_obj.src = img;
        img_obj.classList.add("img_list");
        td_obj.appendChild(img_obj);
    }

    tr_obj.appendChild(td_obj);

    target.appendChild(tr_obj);

    /*最後に、オブジェクトの追加*/
    //this.addObj(tmp_obj);
}
